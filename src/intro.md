# Introduction

MatuushOS is a Linux distribution that has tools, which are mostly written in [Rust](https://rust-lang.org). It is immutable and it is using SquashFS images that are automatically mounted at start-up.

There are two types of images, but we'll talk about them in other section.

Meanwhile, you can look around on this wiki, [contribute to it](https://gitlab.com/MatuushOS/wiki), since it is still in the infancy, or just read it. 

This project started as a school project to get me extra points and got Matus Mastena, the author of this OS and [BDFL](https://en.wikipedia.org/wiki/Benevolent_dictator_for_life) of this project, to his dream middle school.

I, Matus Mastena, will recommend some pages to read:
- [General system management](general/system-management.html)
- [Writing build scripts](general/writing-bscripts.html)
- [Custom OS/update image building](general/custom-image/html)

## Wiki layout

This wiki is rendered via [mdBook](https://github.com/rust-lang/mdbook), so it looks very similar to the [Rust book](https://doc.rust-lang.org/book/). It is written via Markdown that is automatically rendered through mdBook on push to the Git repository via GitLab CI, that'll push the rendered HTML page to GitLab Pages.
