# Choosing the installtion media

If you read the previous chapter of this installation part of this wiki, then you probably understood that this Linux distribution isn't meant for prime-time. But if you still want to do it, you can.

But let's go back to choosing the installation media. I personally recommend [SystemRescueCD](https://system-rescue.org) for a lot of reasons, which we won't discuss here for obvious reasons. But you can also use any Linux ISO you want. Download it, put it in the VM (or on thumb drive), boot it, set up network, and you're done.
