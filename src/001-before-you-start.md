# Before you start

You want to install MatuushOS on real hardware or to VM, don't you?

> *Note:* The following sentences are meant for people, who want to install this OS on real hardware (or bare metal, as some might say it).

Be prepared that it'll be a bumpy ride.

## Hardware support

MatuushOS doesn't support **anything that is NOT X86_64**. This means:
- That non-Intel Macs will not work (unless someone adds a build script for the [Asahi Linux kernel](https://github.com/AsahiLinux/linux))
  - You can also build the kernel yourself and hijack it to the system image, but this requires at least intermediate Linux knowledge.
- That Raspberry Pis will also not work (but if you have a Pi, than you have at least intermediate knowledge of how Linux kernel work).
- That some features (like image downloading and auto-mounting) won't be available, since we have build infrastructure, **but we don't have space to store these built images**.
  - If you want to, you can contact BDFL [on Matrix](https://matrix.to/@tajpekmatus:matrix.org#/@tajpekmatus:matrix.org)
## This project is still in its begginings.

Don't expect that everything will be available. Even though first OS image is planned to drop on July 1st this year, it won't be meant for general audience. It'll be meant for those who understand how to install a Linux distribution without some kind of installer. It is rolling release, which also isn't ideal for the average user.

## CI status
This project is very CI driven. Here's the pipeline status:
- [`image-pm`](https://gitlab.com/MatuushOS/image-pm): [![pipeline status](https://gitlab.com/matuushos/image-pm/badges/main/pipeline.svg)](https://gitlab.com/matuushos/image-pm/-/commits/main) 
- [This wiki](https://gitlab.com/MatuushOS/wiki): [![pipeline status](https://gitlab.com/matuushos/wiki/badges/main/pipeline.svg)](https://gitlab.com/matuushos/wiki/-/commits/main)
