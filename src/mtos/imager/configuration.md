# Configuration reference

Here's the configuration reference for the file. Even though you have everything in a template, it is good to know, which parameter has which type, what it does, and where it is defined.

So let's go over them.

## `name`
- Description: Sets the name of the package.
- Type: Rust `String`
- Defined in: [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#L90), line 90.
## `description`
- Description: Sets the description of package.
- Type: Rust `Vec<String>`
- Defined in [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#L92), line 92.
## `type_of_payload`
- Description: Sets the type of payload.
  - They have their own properties.
  - Possible values are:
    - `OSImage`
    - `Package`
- Type: Internal Rust enum `TypeOfPayload`
- Defined in: [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#L94) and in [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#L37), lines 94 and 37.
## `version`
- Type: Rust `Vec<i32>`
- Description: It sets version of package. The array doesn't support additional non-numeric characters, though.
- Defined in: [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#L96), line 96.
## `sources`
- Type: Rust `Vec<String>`
- Description: Fetches the sources in the `for` loop.
- Defined in: [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#L98), line 98.
## `build`
- Type: Internal Rust struct `Building`
- Description: Instructions for `imager` to build the package.
- Defined in: [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#L81), line 81.
### Fields of `build`
#### `prepare`, `build`, `install`
- Common type: Rust `Vec<String>`
- Description: Runs the commands defined in the arrays.
  - First element of the array will be selected as the run command, everything else is iterated as arguments to prepare/build/install command. 
## `localdeps`
- Type: Rust `Vec<String>`
- Description: Finds the local dependencies in the path defined in the arrays and builds them **before** actual package for dependency resolution
- Defined in: [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#L106), line 106.
## `is_git`
- Type: Rust `Vec<String>`
- Description: Switch to use Git repository instead of tarball.
- Defined in: [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#112), line 112.
## `filenames`
- Type: Rust: `Vec<String>`
- Description: Filenames of downloaded tarballs for extracting.
- Defined in: [`crates/imager/src/utils.rs`](https://gitlab.com/matuushos/image-pm/-/blob/main/crates/imager/src/utils.rs?ref_type=heads#L115), line 115.
