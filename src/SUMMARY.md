[Introduction](intro.md)

# Installation

- [Before you start](001-before-you-start.md)

- [Choosing the installation media](002-Installation-media.md)

- [Drive formatting and mounting](003-drive-formatting.md)

- [Downloading the required files](004-downloading-files.md)
  - [Choosing the right image](0041-install-media.md)

- [Installing bootloader](005-bootloader.md)

- [Adding the user](006-bootloader.md)

- [`/mtos/config`](007-configuration.md)

- [Finishing up](008-finishing-up.md)

# The wiki stuff

- [System management](general/system-management.md)

- [Setting up Docker Swarm cluster](general/docker-swarm.md)

- [Writing build scripts](general/writing-bscripts.md)

- [Setting up custom repository](general/custom-repository.md)

- [Custom image building](general/custom-image.md)

- [Support for languages other than Rust](general/langs-other-than-rust.md)

# The MatuushOS ecosystem
- [About it](mtos/about.md)
- [Imager](mtos/imager.md)
   - [Inspiration](mtos/imager/inspiration.md)
   - [Configuration reference](mtos/imager/configuration.md)
- [IPM](mtos/ipm.md)
   - [Inspiration](mtos/ipm/inspiration.md)
   - [Configuration reference](mtos/ipm/configuration.md)




-------
[Contributors](CONTRIBUTORS)
