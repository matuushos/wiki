# Drive formatting and mounting

If you've finished the previous step, then you can format the drive or drives. The recommended partition layout is 512 megabytes for the boot partition, and rest for the root partition.
